

import Foundation
import CoreLocation
import UIKit

class Utils {
    
    
    func isValidEmail(_ email:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: email)
    }
    
    static func getDistanceInMeterBetween(from: CLLocationCoordinate2D, to: CLLocationCoordinate2D) -> Double {
        let fromCLLocation = CLLocation(latitude: from.latitude, longitude: from.longitude)
        let toCLLocation = CLLocation(latitude: to.latitude, longitude: to.longitude)
        return toCLLocation.distance(from: fromCLLocation)
    }
    
   
    static func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size
        let widthRatio  = targetSize.width  / image.size.width
        let heightRatio = targetSize.height / image.size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
    
        return newImage!
    }
}

extension Dictionary {
    
    // Build string representation of HTTP parameter dictionary of keys and objects
    // returns string representation in the form of key1=value1&key2=value2 where the keys and values are percent escaped
    
    func stringFromHttpParameters() -> String {
        var parametersString = ""
        for (key, value) in self {
            if let key = key as? String,
                let value = value as? String {
                parametersString = parametersString + key + "=" + value + "&"
            }
        }
        parametersString = parametersString.substring(to: parametersString.index(before: parametersString.endIndex))
        return parametersString.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
    }
}


extension Date{
    func convertDatetoString(format: String) -> String{
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        let dateString = dateFormatter.string(from: self)
        return dateString
        
    }
}


import UIKit

extension CALayer {
    
    func setShadowUIColor(color: UIColor){
        self.shadowColor = color.cgColor
    }
}


extension UIView {
    var frameMinX: CGFloat { return self.frame.minX }
    var frameMinY: CGFloat { return self.frame.minY }
    var frameMidX: CGFloat { return self.frame.midX }
    var frameMidY: CGFloat { return self.frame.midY }
    var frameMaxX: CGFloat { return self.frame.maxX }
    var frameMaxY: CGFloat { return self.frame.maxY }
    var frameMin: CGPoint  { return CGPoint(x:frameMinX, y:frameMinY) }
    var frameMid: CGPoint  { return CGPoint(x:frameMidX, y:frameMidY) }
    var frameMax: CGPoint  { return CGPoint(x:frameMaxX, y:frameMaxY) }
}


extension UIView {
    
    func dropShadow() {
        
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.5
        self.layer.shadowOffset = CGSize(width: 1, height: 1)
        self.layer.shadowRadius = 1
        
        self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        self.layer.shouldRasterize = true
    }
}



