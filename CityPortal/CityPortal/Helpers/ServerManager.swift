//
//  ServerManager.swift
//  CityPortal
//
//  Created by El Moatasem on 7/22/17.
//  Copyright © 2017 El Moatasem. All rights reserved.
//

import Foundation
import Foundation
import AFNetworking
import Alamofire
import CoreLocation

class ServerManager{
    
    typealias CompletionBlock = (APIResponse?) -> Void
    func getCityData(_ pageId : Int, handler: @escaping CompletionBlock) -> Void {
        
        let manager = AFHTTPSessionManager()
        let cityUrl = String(format:  ServerUrls.CITY_URL, pageId)
        print(cityUrl)
        let result = APIResponse()
        manager.get(
            cityUrl,
            parameters: nil,
            success:
            {
                (operation, responseObject) in
                print(responseObject)
                
                if let jsonResponse = responseObject as? [[String: Any]]{
                    let cities = self.parseCities(cityJson: jsonResponse)
                    result.data = cities as AnyObject?
                    handler(result)
                }
        },
            failure:
            {
                (operation, error) in
                result.error = error
                handler(result)
                print("Error: " + error.localizedDescription)
        })
        
    }
    
    
    private func parseCities(cityJson: [[String: Any]]) -> [CityInfo]{
        var result: [CityInfo] = [CityInfo]()
        for dict in cityJson {
            let city:CityInfo = CityInfo()
            if let cityId = dict[JsonKeys.CITY_ID_KEY] as? String{
                city.cityId = cityId
            }
            if let cityName = dict[JsonKeys.CITY_NAME_KEY] as? String{
                city.cityName = cityName
            }
            if let countryCode = dict[JsonKeys.COUNTRY_CODE_KEY] as? String{
                city.countryCode = countryCode
            }
            if let coordDict = dict[JsonKeys.CITY_COORD_KEY] as? [String: Any] {
                if let lat = coordDict[JsonKeys.CITY_LAT_KEY] as? String, let lng = coordDict[JsonKeys.CITY_LON_KEY] as? String{
                    city.cityLocation = CLLocationCoordinate2DMake(Double(lat)!,Double(lng)!)
                }
            }
            
            result.append(city)
        }
        return result
    }
    
}
