
import MagicalRecord
import CoreLocation

class DBManager {
    

    enum CityEntityAttributes {
        static let CityID = "cityID"
        static let CityName = "cityName"
        static let CountryCode = "countryCode"
        static let PageId = "pageId"
        static let CityLng = "cityLng"
        static let CityLat = "cityLat"
        static let RecordID = "recordID"
    }
    
    
    static let sharedInstance: DBManager = {
        let instance = DBManager()
        return instance
    }()
    

    func clearSearchHistory() {
        CityEntity.mr_truncateAll()
    }
    
    private func getMaxInsertedRecord() -> Int64{
        let maxId : Int64 = Int64(CityEntity.mr_aggregateOperation("max:", onAttribute: "recordID", with: nil) as! NSNumber)
        return maxId
    }
    
    
    func addCitiesToDB(cities: [CityInfo],pageId:Int){
        for city in cities{
            createOrUpdateCityRecord(city: city,pageId: pageId)
        }
    }
    
    private func createOrUpdateCityRecord(city: CityInfo,pageId:Int)  {
        let entry:CityEntity? = self.fetchCityWithId(cityId: city.cityId!)
        if let entry = entry {
           self.updateEntry(cityRecord:entry,city: city,pageId: pageId)
        }
        else{
            self.createEntry(city: city, pageId: pageId)
        }
    }
    
    private func updateEntry(cityRecord: CityEntity,city: CityInfo,pageId: Int) {
        cityRecord.pageId = Int64(pageId)
        cityRecord.cityName = city.cityName
        cityRecord.countryCode = city.countryCode
        cityRecord.cityLat = (city.cityLocation?.latitude)!
        cityRecord.cityLng = (city.cityLocation?.longitude)!
        saveContext()
    }
    
    private func createEntry(city: CityInfo,pageId: Int) {
        let cityRecord = CityEntity.mr_createEntity()
        cityRecord?.recordID = getMaxInsertedRecord() + 1
        cityRecord?.pageId = Int64(pageId)
        cityRecord?.cityID = city.cityId
        cityRecord?.cityName = city.cityName
        cityRecord?.countryCode = city.countryCode
        cityRecord?.cityLat = (city.cityLocation?.latitude)!
        cityRecord?.cityLng = (city.cityLocation?.longitude)!
        saveContext()
    }
    
    
    func fetchCityWithId(cityId:String) -> CityEntity?{
        let predicate = NSPredicate(format: CityEntityAttributes.CityID + " == \(cityId)")
        let queryResult : [CityEntity] = CityEntity.mr_findAll(with: predicate) as! [CityEntity]
        var cityRecord:CityEntity? = nil
        if(queryResult.count > 0){
            cityRecord = queryResult[0]
        }
        return cityRecord
    }
    
    
    func retrieveAllCities() -> [CityInfo]{
        let citiesEntities = retrieveAllCitiesEntities()
        var cities: [CityInfo] = [CityInfo]()
        for cityEntity in citiesEntities{
            let city = CityInfo()
            city.cityId = cityEntity.cityID
            city.cityName = cityEntity.cityName
            city.countryCode = cityEntity.countryCode
            city.cityLocation = CLLocationCoordinate2DMake(Double(cityEntity.cityLat),Double(cityEntity.cityLng))
            cities.append(city)
        }
     return cities
    }
    
    func retrieveAllCitiesEntities() -> [CityEntity] {
        let sortKey = CityEntityAttributes.RecordID
        let queryRes = CityEntity.mr_findAllSorted(by: sortKey, ascending: true, with: nil)
        return queryRes as? [CityEntity] ?? []
    }
    
    private func saveContext() {
        NSManagedObjectContext.mr_default().mr_saveToPersistentStoreAndWait()
    }
}
