//
//  MapViewController.swift
//  CityPortal
//
//  Created by El Moatasem on 7/22/17.
//  Copyright © 2017 El Moatasem. All rights reserved.
//

import Foundation
import UIKit
import GoogleMaps

class MapViewController: AbstractViewController{
    
    @IBOutlet weak var mapView: GMSMapView!
    
    var currentCity: CityInfo? = nil
    var cityLocationMarker : GMSMarker? = nil
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        mapView.delegate = self
        
        self.cityLocationMarker = createLocationAnnotation(withLocation: currentCity?.cityLocation, snippet: "", image: nil)
        let mapCamera = GMSCameraPosition.camera(withLatitude: (currentCity?.cityLocation?.latitude)!, longitude: (currentCity?.cityLocation?.longitude)!, zoom: 12)
        mapView?.camera = mapCamera
        
        self.title = currentCity?.cityName
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    
    func createLocationAnnotation(withLocation location: CLLocationCoordinate2D?, snippet: String, image: UIImage?) -> GMSMarker? {
        let marker = GMSMarker()
        marker.position = location!
        marker.snippet = snippet
        marker.appearAnimation = GMSMarkerAnimation.pop
        marker.map = mapView
        marker.icon = image
        return marker
    }
}


extension MapViewController : GMSMapViewDelegate{
    public func mapView(_ mapView: GMSMapView, willMove gesture: Bool){
        
    }
    
    public func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition){
        
    }
    
    public func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition){
        
    }
    
}
