import UIKit
import ReachabilitySwift
import MBProgressHUD
import IQKeyboardManagerSwift

class AbstractViewController: UIViewController {
    
    
    var hasDoneButton = false
    var hasBackButton = false
    var viewControllerName = "Title"
    
    var rightViewItem: UIView?
    var leftViewItem: UIView?
    var hud:MBProgressHUD? = nil
    let reachability = Reachability()!
    
    
    
    enum StoryBoard {
        static let SettingVCID = "Main"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addTapGesture()
        
    }
    
    func addTapGesture(){
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(AbstractViewController.hideKeyboard))
        tapGesture.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tapGesture)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        IQKeyboardManager.sharedManager().enable = true
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: NSNotification.Name.UIKeyboardDidHide, object: nil)
        addReachabilityNoticiation()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardDidHide, object: nil)
        removeReachabilityNotifications()
    }
    
    
    // MARK: UI helper methods
    
    fileprivate func initNavigationBarStyle(){
        if self.navigationController != nil {
            //            self.navigationController!.navigationBar.barStyle = UIBarStyle.black
            //            self.navigationController!.navigationBar.barTintColor = Constants().NAV_BAR_COLOR
        }
        
    }
    
    
    
    fileprivate func createNavBarViews() {
        
        if hasDoneButton {
            
            let button: UIButton = UIButton()
            button.setTitle("Done", for: UIControlState())
            button.sizeToFit()
            button.addTarget(self, action: #selector(AbstractViewController.dismissView), for: UIControlEvents.touchUpInside)
            
            addRightNavigationButton(button)
        }
    }
    
    fileprivate func addLeftNavigationButton(_ button: UIButton) {
        let leftItem:UIBarButtonItem = UIBarButtonItem()
        leftItem.customView = button
        self.navigationItem.leftBarButtonItem = leftItem
    }
    
    fileprivate func addRightNavigationButton(_ button: UIButton) {
        let rightItem:UIBarButtonItem = UIBarButtonItem()
        rightItem.customView = button
        self.navigationItem.rightBarButtonItem = rightItem
    }
    
    
    
    func dismissView(){
        self.dismiss(animated: true, completion: nil)
    }
    
    
    //MARK: Helper Methods :
    
    func showLoading(_ message:String){
        self.hud =  MBProgressHUD.showAdded(to: self.view, animated: true)
        self.hud!.label.text = message
        
    }
    
    func showCustomLoadingWithButton(_ message:String){
        self.hud =  MBProgressHUD.showAdded(to: self.view, animated: true)
        self.hud!.label.text = message
        self.hud!.button.setImage( UIImage(named:"cancelIcon"), for: UIControlState())
        self.hud!.button.addTarget(self, action:#selector(self.cancelLoading), for: .touchUpInside)
        self.hud?.button.layer.borderWidth = 0.0;
    }
    
    func hideLoading(){
        MBProgressHUD.hide(for: self.view, animated: true)
    }
    
    func cancelLoading() {
        self.hideLoading()
    }
    
    //MARK: Alert
    func showToast(_ message:String){
        self.hud =  MBProgressHUD.showAdded(to: self.view, animated: true)
        self.hud?.mode = MBProgressHUDMode.text;
        self.hud?.detailsLabel.text = message;
        self.hud?.margin = 10;
        self.hud?.offset.y = 150;
        self.hud?.removeFromSuperViewOnHide = true;
        self.hud?.isUserInteractionEnabled = true
        self.hud?.hide(animated:true , afterDelay: 3)
    }
    
    
    
    func presentAlert(title: String, message: String, actionButtonsTitle: [String], actions: [(() -> ())], userInfo: AnyObject?) -> UIAlertController? {
        return self.presentAlertWithCanelOption(title: title, message: message, actionButtonsTitle: actionButtonsTitle, actions: actions, userInfo: userInfo, hasCancelAction: false)
    }
    
    
    func presentAlertWithCanelOption(title: String, message: String, actionButtonsTitle: [String], actions: [(() -> ())], userInfo: AnyObject?,hasCancelAction: Bool) -> UIAlertController? {
        
        let alertView = UIAlertController(title: "", message: message, preferredStyle: .alert)
        for actionIdx in 0..<actionButtonsTitle.count {
            let action = UIAlertAction(title: actionButtonsTitle[actionIdx], style: UIAlertActionStyle.default, handler: {(alert: UIAlertAction!) -> Void in
                actions[actionIdx]()
            })
            
            alertView.addAction(action)
        }
        if(hasCancelAction){
            alertView.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
        }
        
        self.present(alertView, animated: true, completion: nil)
        
        return alertView
    }
    
    
    
    
    func addReachabilityNoticiation(){

        NotificationCenter.default.addObserver(self, selector: #selector(self.reachabilityChanged),name: ReachabilityChangedNotification,object: reachability)
        do{
            try reachability.startNotifier()
        }catch{
            print("could not start reachability notifier")
        }
    }
    
    func removeReachabilityNotifications(){
        reachability.stopNotifier()
        NotificationCenter.default.removeObserver(self,
                                                  name: ReachabilityChangedNotification,
                                                  object: reachability)
    }
    
    
    func reachabilityChanged(note: Notification) {
        
        let reachability = note.object as! Reachability
        handleReachabilityChange(isReachable:reachability.isReachable)
        
        if reachability.isReachable {
            if reachability.isReachableViaWiFi {
                print("Reachable via WiFi")
            } else {
                print("Reachable via Cellular")
            }
        } else {
            print("Network not reachable")
        }
    }
    
    func handleReachabilityChange(isReachable: Bool){
        
    }
    
    // MARK: Keyboard delegates
    func keyboardWillShow(notification: NSNotification) {
        
    }
    
    func keyboardWillHide(notification: NSNotification) {
        
    }
    
    
    // MARK: Connection issues handler
    func isReachableToInternet() -> Bool {
        return Reachability()!.currentReachabilityStatus != .notReachable
    }
    
    
    func hideKeyboard() {
        self.view.endEditing(true)
    }
    
}

