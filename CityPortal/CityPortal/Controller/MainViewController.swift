//
//  MainViewController.swift
//  CityPortal
//
//  Created by El Moatasem on 7/22/17.
//  Copyright © 2017 El Moatasem. All rights reserved.
//

import Foundation
import UIKit


enum FilterType : Int{
    case DEFAULT_FILTER = 0
    case COUNTRY_FILTER = 1
    case CITY_FILTER = 2
}

enum ActionMode : Int{
    case NORMAL_MODE = 0
    case SEARCH_MODE = 1
    case OFFLINE_MODE = 2
}

class MainViewController: AbstractViewController{
    
    
    enum UIConstants {
        static let SearchCityText = "Search City"
        static let mapViewSegueId = "mapViewSegueId"
    }
    
    // UI :
    var searchBar: UISearchBar!
    @IBOutlet weak var citiesTableView: UITableView!
    var resultSearchController = UISearchController()
    @IBOutlet weak var filterView: UISegmentedControl!
    
    //Data :
    //    var originalCityList:[CityInfo]? = [CityInfo]()
    //    var searchableCityList:[CityInfo]? = [CityInfo]()
    //    var filteredCityList:[CityFilterObject]? = [CityFilterObject]()
    
    
    var citiesResult:[[CityInfo]]? = [[CityInfo]]()
    var keysSet:[String]? = [String]()
    
    
    var viewModel: CityDataViewModel? = nil
    var selectedCity : CityInfo? = nil
    var pageId = 0
    var currentFilterType:FilterType = FilterType.DEFAULT_FILTER
    var currentMode:ActionMode = ActionMode.NORMAL_MODE
    var wasOffline:Bool = false
    
    
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        setUpUI()
        setUpData()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
    }
    
    
    let DEFAULT_ROW_HEIGHT = 80
    private func setUpUI(){
        self.title = ("City Portal")
        self.resultSearchController = ({
            let controller = UISearchController(searchResultsController: nil)
            controller.searchResultsUpdater = self
            controller.delegate = self
            controller.searchBar.delegate = self
            controller.searchBar.sizeToFit()
            controller.dimsBackgroundDuringPresentation = false
            definesPresentationContext = true
            return controller
        })()
        self.searchBar = self.resultSearchController.searchBar
        self.citiesTableView.tableHeaderView = self.resultSearchController.searchBar
        
        self.filterView.addTarget(self, action: #selector(MainViewController.segmentedValueChanged(_:)), for: .valueChanged)
        //        self.citiesTableView.estimatedRowHeight = CGFloat(DEFAULT_ROW_HEIGHT)
        //        self.citiesTableView.rowHeight = UITableViewAutomaticDimension
    }
    
    
    private func setUpData(){
        viewModel = CityDataViewModel()
        viewModel?.delegate = self
        pageId = 1
        viewModel?.getCityDataWithPageId(pageId: pageId)
    }
    
    
    func segmentedValueChanged(_ sender:UISegmentedControl!)
    {
        self.currentFilterType = FilterType(rawValue :sender.selectedSegmentIndex)!
        self.viewModel?.setFilterType(filterType: self.currentFilterType)
    }
    
    
    override func handleReachabilityChange(isReachable: Bool){
        if(isReachable){
            self.currentMode = ActionMode.NORMAL_MODE
            if(wasOffline){
                DispatchQueue.main.async {
                    self.citiesTableView.reloadData()
                    self.showToast("Internet Connection Restored")
                }
            }
            DispatchQueue.main.async {
                self.citiesTableView.reloadData()
            }
            wasOffline = false
        }
        else{
            self.currentMode = ActionMode.OFFLINE_MODE
            DispatchQueue.main.async {
                self.citiesTableView.reloadData()
                self.showToast("No Internet Connection")
            }
            wasOffline = true
            
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == UIConstants.mapViewSegueId {
            let mapVC = segue.destination as! MapViewController
            mapVC.currentCity = self.selectedCity
        }
    }
    
    
}

//MARK: - UITableViewDataSource - UITableViewDelegate
extension MainViewController: UITableViewDataSource, UITableViewDelegate {
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if((self.citiesResult?.count)! > 0){
            if(section == (self.citiesResult?.count)! - 1 && self.currentMode == ActionMode.NORMAL_MODE){
                return (self.citiesResult?[section].count)! + 1
            }
            else{
                return (self.citiesResult?[section].count)!
            }
        }
        return 0
    }
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return (self.citiesResult?.count)!
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if(indexPath.row <  (self.citiesResult?[indexPath.section].count)!){
            let cellIdentifier = "cityCellId"
            let cityDataCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? CityDataCell
            cityDataCell?.cityInfo = self.citiesResult?[indexPath.section][indexPath.row]
            cityDataCell?.bind()
            return cityDataCell!
        }
        else{
            
            print("self.currentMode",self.currentMode)
            pageId += 1
            viewModel?.getCityDataWithPageId(pageId: pageId)
            let cellIdentifier = "loadingCellId"
            let loadingCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? LoadingMoreCell
            loadingCell?.bind()
            return loadingCell!
            
        }
        
    }
    
    public func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String?{
        return self.keysSet?[section]
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(DEFAULT_ROW_HEIGHT)
    }
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        self.selectedCity = self.citiesResult?[indexPath.section][indexPath.row]
        self.performSegue(withIdentifier: UIConstants.mapViewSegueId, sender: self)
    }
}


extension MainViewController: UISearchResultsUpdating,UISearchControllerDelegate,UISearchBarDelegate{
    // MARK: - UISearchResultsUpdating
    public func updateSearchResults(for searchController: UISearchController){
        //        self.currentMode = ActionMode.SEARCH_MODE
        //         print("here>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
        self.viewModel?.handleSearchResult(keyword: searchController.searchBar.text!)
    }
    // MARK: - UISearchBarDelegate
    
    public func searchBarTextDidBeginEditing(_ searchBar: UISearchBar){
        self.currentMode = ActionMode.SEARCH_MODE
    }
    public func searchBarTextDidEndEditing(_ searchBar: UISearchBar){
        self.currentMode = ActionMode.NORMAL_MODE
    }
    public func searchBarCancelButtonClicked(_ searchBar: UISearchBar){
        self.currentMode = ActionMode.NORMAL_MODE
        self.citiesTableView.reloadData()
        searchBar.resignFirstResponder()
    }
    
    // MARK: - UISearchControllerDelegate
    func presentSearchController(_ searchController: UISearchController) {
    }
    
    func willPresentSearchController(_ searchController: UISearchController) {
    }
    
    func didPresentSearchController(_ searchController: UISearchController) {
    }
    
    func willDismissSearchController(_ searchController: UISearchController) {
    }
    
    func didDismissSearchController(_ searchController: UISearchController) {
    }
}




extension MainViewController: CityDataDelegate{
    func didReceiveDataUpdate(){
        citiesResult = viewModel?.cityResult
        keysSet = viewModel?.keysSet
        self.citiesTableView.reloadData()
    }
    func didReceiveErrorResponse(_ error:Error){
        DispatchQueue.main.async {
            self.pageId -= 1
            self.showToast(error.localizedDescription)
        }
    }
}

