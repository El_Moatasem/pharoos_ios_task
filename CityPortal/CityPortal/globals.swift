//
//  globals.swift
//  CityPortal
//
//  Created by El Moatasem on 7/22/17.
//  Copyright © 2017 El Moatasem. All rights reserved.
//

import Foundation


enum ServerUrls {
    static let BASE_URL        = "http://assignment.pharos-solutions.de"
    static let CITY_ENDPOINT   = "cities.json"
    static let CITY_URL        = BASE_URL + "/" + CITY_ENDPOINT + "?page=%d"
}


enum JsonKeys {
    static let COUNTRY_CODE_KEY    = "country"
    static let CITY_NAME_KEY       = "name"
    static let CITY_ID_KEY         = "_id"
    static let CITY_COORD_KEY      = "coord"
    static let CITY_LON_KEY        = "lon"
    static let CITY_LAT_KEY        = "lat"
}


enum Constants{
    static let NO_INTERNET_HEADER = "No Internet"
    static let NO_INTERNET_MESSAGE = "Failed to connect."
    static let INTERNET_CONNECTION_RESTORED = "Internect Connection Restored"
    static let NO_CONNECTION_NOTIFICATION = "noConnection"
}


public enum ServiceKeys{
    static let GOOGLE_MAPS_API_KEY = "AIzaSyAClW85mKQW1Q29015vnVF_8sT2YdOz3Dw"
}



