//
//  CityInfo.swift
//  CityPortal
//
//  Created by El Moatasem on 7/22/17.
//  Copyright © 2017 El Moatasem. All rights reserved.
//

import Foundation
import CoreLocation

class CityInfo: NSObject {
    var cityId: String?
    var cityName: String?
    var countryCode: String?
    var cityLocation: CLLocationCoordinate2D? = nil
    
    
}
