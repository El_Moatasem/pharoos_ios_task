//
//  CityEntity+CoreDataProperties.swift
//  
//
//  Created by El Moatasem on 7/22/17.
//
//  This file was automatically generated and should not be edited.
//

import Foundation
import CoreData


extension CityEntity {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CityEntity> {
        return NSFetchRequest<CityEntity>(entityName: "CityEntity");
    }

    @NSManaged public var cityID: String?
    @NSManaged public var cityLat: Double
    @NSManaged public var cityLng: Double
    @NSManaged public var cityName: String?
    @NSManaged public var countryCode: String?
    @NSManaged public var pageId: Int64
    @NSManaged public var recordID: Int64

}
