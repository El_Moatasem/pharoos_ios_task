//
//  APIResponse.swift
//  CarpoolPassenagerApp
//
//  Created by El Moatasem on 10/23/16.
//  Copyright © 2016 El Moatasem. All rights reserved.
//

import Foundation


open class APIResponse {
    var data : AnyObject?
    var error : Error?
    var status : NSInteger = 0


}
