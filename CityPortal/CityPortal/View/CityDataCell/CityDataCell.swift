//
//  CityDataCell.swift
//  CityPortal
//
//  Created by El Moatasem on 7/22/17.
//  Copyright © 2017 El Moatasem. All rights reserved.
//

import Foundation
import UIKit
import GoogleMaps

class CityDataCell: UITableViewCell {
    
    
    @IBOutlet weak var countryFlagImageView: UIImageView!
    @IBOutlet weak var cityNameLabel: UILabel!
    @IBOutlet weak var mapView: GMSMapView!
    var cityInfo: CityInfo? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    
    func bind() {
        self.cityNameLabel.text = self.cityInfo?.cityName
        self.countryFlagImageView.image = UIImage(named: (self.cityInfo?.countryCode)!)
        let mapCamera = GMSCameraPosition.camera(withLatitude: (cityInfo?.cityLocation?.latitude)!, longitude: (cityInfo?.cityLocation?.longitude)!, zoom: 12)
        mapView?.camera = mapCamera
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}

