//
//  LoadingMoreCell.swift
//  CityPortal
//
//  Created by El Moatasem on 7/22/17.
//  Copyright © 2017 El Moatasem. All rights reserved.
//


import Foundation
import UIKit
import GoogleMaps

class LoadingMoreCell: UITableViewCell {
    
    @IBOutlet weak var loadingView: UIActivityIndicatorView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    
    func bind() {
        self.loadingView.startAnimating()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
