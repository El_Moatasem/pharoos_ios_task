//
//  CityDataViewModel.swift
//  CityPortal
//
//  Created by El Moatasem on 7/22/17.
//  Copyright © 2017 El Moatasem. All rights reserved.
//

import Foundation

public protocol CityDataDelegate :NSObjectProtocol {
    
    func didReceiveDataUpdate()
    func didReceiveErrorResponse(_ error:Error)
    
}

class CityDataViewModel: NSObject {
    
    // private :
    private var originalCityList:[CityInfo]? = [CityInfo]()
    private var searchableCityList:[CityInfo]? = [CityInfo]()
    private var filteredCityList:[CityFilterObject]? = [CityFilterObject]()
    
    
    // public :
    var delegate : CityDataDelegate?
    var currentFilterType:FilterType = FilterType.DEFAULT_FILTER
    var cityResult:[[CityInfo]]? = [[CityInfo]]()
    var keysSet:[String]? = [String]()
    
    
    // Get Data From Server :
    func getCityDataWithPageId(pageId: Int){
        ServerManager().getCityData(pageId) {
            (response) -> Void in
            
            if response?.error != nil {
                self.delegate?.didReceiveErrorResponse((response?.error)!)
                self.retrieveCitiesfromDB()
            }
            else{
                let cities: [CityInfo] = (response?.data as! [CityInfo]?)!
                self.originalCityList?.append(contentsOf: cities)
                self.searchableCityList?.append(contentsOf: cities)
                DispatchQueue.main.async {
                    DBManager.sharedInstance.addCitiesToDB(cities: cities, pageId: pageId)
                    self.filterCityList()
                }
            }
        }
    }
    
    func retrieveCitiesfromDB(){
        self.originalCityList?.removeAll()
        self.searchableCityList?.removeAll()
        let cities = DBManager.sharedInstance.retrieveAllCities()
        self.originalCityList?.append(contentsOf: cities)
        self.searchableCityList?.append(contentsOf: cities)
        DispatchQueue.main.async {
            self.filterCityList()
        }
        
    }

    
    func setFilterType(filterType: FilterType){
        self.currentFilterType = filterType
        filterCityList()
    }
    
    // Format Data with Filter and Search :
    func filterCityList(){
        
        filteredCityList?.removeAll()
        switch self.currentFilterType {
            
        case .DEFAULT_FILTER:
            let filterObject:CityFilterObject = CityFilterObject()
            filterObject.key = "All"
            filterObject.cityList = self.searchableCityList!
            filteredCityList?.append(filterObject)
            break
            
        case .COUNTRY_FILTER:
            var copiedList = [CityInfo]()
            for city in searchableCityList!{
                copiedList.append(city)
            }
            let sortedList = copiedList.sorted { ($0).countryCode! < ($1).countryCode! } as NSArray
            var lastCode = ""
            var filterObject:CityFilterObject? = nil
            for city in sortedList{
                
                if((city as! CityInfo).countryCode != lastCode){
                    filterObject = CityFilterObject()
                    filteredCityList?.append(filterObject!)
                    filterObject?.key = (city as! CityInfo).countryCode
                    if(filterObject?.cityList == nil){
                        filterObject?.cityList = [CityInfo]()
                    }
                    filterObject?.cityList?.append(city as! CityInfo)
                }
                else{
                    filterObject?.cityList?.append(city as! CityInfo)
                }
                lastCode = (city as! CityInfo).countryCode!
            }
            break
            
        case .CITY_FILTER:
            var copiedList = [CityInfo]()
            for city in searchableCityList!{
                copiedList.append(city)
            }
            let sortedList = copiedList.sorted { ($0).cityName! < ($1).cityName! } as NSArray
            var lastChar = ""
            var filterObject:CityFilterObject? = nil
            for city in sortedList{
                let cityName = (city as! CityInfo).cityName
                let firstChar = (cityName?[(cityName?.startIndex)!])
                let firstCharString = "\(firstChar!)"
                
                if(firstCharString != lastChar){
                    filterObject = CityFilterObject()
                    filteredCityList?.append(filterObject!)
                    
                    filterObject?.key = firstCharString
                    if(filterObject?.cityList == nil){
                        filterObject?.cityList = [CityInfo]()
                    }
                    filterObject?.cityList?.append(city as! CityInfo)
                }
                else{
                    filterObject?.cityList?.append(city as! CityInfo)
                }
                lastChar = firstCharString
            }
            break
        }
        prepareViewResult()
    }
    
    // Handle Data Search
    func handleSearchResult(keyword: String){
        searchableCityList?.removeAll()
        if(keyword.characters.count == 0){
            for city in originalCityList!{
                searchableCityList?.append(city)
            }
        }
        else{
            for city in originalCityList!{
                if(city.cityName?.hasPrefix(keyword))!{
                    searchableCityList?.append(city)
                }
            }
        }
        filterCityList()
    }
    
    
    func prepareViewResult(){
        cityResult?.removeAll()
        keysSet?.removeAll()
        for filterObject in filteredCityList!{
            cityResult?.append(filterObject.cityList!)
            keysSet?.append(filterObject.key!)
        }
        self.delegate?.didReceiveDataUpdate()
    }
    
}

