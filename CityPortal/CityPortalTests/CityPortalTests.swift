//
//  CityPortalTests.swift
//  CityPortalTests
//
//  Created by El Moatasem on 7/22/17.
//  Copyright © 2017 El Moatasem. All rights reserved.
//

import XCTest
@testable import CityPortal

class CityPortalTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    
    func testServerCall(){
        ServerManager().getCityData(1) {
            (response) -> Void in
            
            if response?.error != nil {
            }
            else{
                let cities: [CityInfo] = (response?.data as! [CityInfo]?)!
                XCTAssert((cities.count) > 0)
            }
        }
    }
    
    
    func testDBCall(){
        let cities = DBManager.sharedInstance.retrieveAllCities()
        XCTAssert((cities.count) > 0)
        
    }
    
}
