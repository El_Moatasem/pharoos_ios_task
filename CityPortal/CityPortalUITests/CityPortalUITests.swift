//
//  CityPortalUITests.swift
//  CityPortalUITests
//
//  Created by El Moatasem on 7/22/17.
//  Copyright © 2017 El Moatasem. All rights reserved.
//

import XCTest

class CityPortalUITests: XCTestCase {
        
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testSuccessSearch() {
        let app = XCUIApplication()
        app.tables.searchFields["Search"].tap()
        app.searchFields["Search"].typeText("F")
        let cell = app.tables.element.cells.element(boundBy: 0)
        XCTAssertTrue(cell.exists)
    }
    
    
    func testFailureSearch() {
        let app = XCUIApplication()
        app.tables.searchFields["Search"].tap()
        app.searchFields["Search"].typeText("abcd")
        let cell = app.tables.element.cells.element(boundBy: 0)
        XCTAssertFalse(cell.exists)
    }
    
    func testCancelSearch(){
        let app = XCUIApplication()
        app.tables.searchFields["Search"].tap()
        app.searchFields["Search"].typeText("Xx")
        app.buttons["Cancel"].tap()
        let cell = app.tables.element.cells.element(boundBy: 0)
        XCTAssertTrue(cell.exists)
    }
    
    
}
